<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use APP\cliente;


class Cliente extends Model
{
    protected $fillable = [
        'nombre',
        'apellido',
        'cedula',
        'direccion',
        'telefono',
        'fecha_nacimiento',
        'email'

    ];
}
