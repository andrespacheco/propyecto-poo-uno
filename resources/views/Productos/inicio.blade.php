@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Modulo de productos</div>
                <div class="col text-right">
                    <a href="{{route('crear.productos')}}" class="btn-sm btn-primary">Nuevo Producto</a>
</div>
                <div class="card-body">
                <table class="table">
  <thead>
    <tr>
      <th scope="col">Id</th>
      <th scope="col">Nombre</th>
      <th scope="col">Tipo</th>
      <th scope="col">Estado</th>
      <th scope="col">Precio</th>
    </tr>
  </thead>
  <tbody>
    @foreach($producto as $item)
    <tr>
      <th scope="row">{{$item->id}}</th>
      <td>{{$item->nombre}}</td>
      <td>{{$item->tipo}}</td>
      <td>{{$item->estado}}</td>
      <td>{{$item->precio}}</td>
    </tr>
    @endforeach
  </tbody>
</table>
              </div>
            </div>
        </div>
    </div>
</div>
@endsection